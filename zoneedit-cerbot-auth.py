#!/usr/bin/env python

import hashlib
import os
import re
import requests
import sys
import time

from datetime import datetime
from dotenv import load_dotenv
from lxml import html


def login_hash(usr, pwd, chal):
    """
    Compute the login hash value.

    :param usr:
        The username
    :param pwd:
        The password
    :param chal:
        The server-generated challenge
    :return:
        The login hash value
    """
    pwhash = hashlib.md5(bytes(pwd, 'utf-8')).hexdigest()
    return hashlib.md5(bytes(usr + pwhash + chal, 'utf-8')).hexdigest()


def input_value(tree, name, default=""):
    """
    Extracts the value of an HTML input element.

    :param tree:
        The HTML document
    :param name:
        The name of the input element
    :param default:
        The default value if the element does not exist
    :return:
        The value of the input element
    """
    return next(iter(tree.xpath(f'//input[@name="{name}"]/@value')), default)


if __name__ == "__main__":

    load_dotenv()

    # certbot manual auth hook variables
    #
    # CERTBOT_DOMAIN: The domain being authenticated
    # CERTBOT_VALIDATION: The validation string

    certbot_domain = os.environ.get('CERTBOT_DOMAIN', None)
    certbot_value = os.environ.get('CERTBOT_VALIDATION', None)

    if not certbot_domain or not certbot_value:
        print(f"ERROR: empty certbot input (CERTBOT_DOMAIN={certbot_domain}, CERTBOT_VALIDATION={certbot_value}).", file=sys.stderr)
        sys.exit(1)

    username = os.environ.get('ZONEEDIT_USERNAME', None)
    password = os.environ.get('ZONEEDIT_PASSWORD', None)

    if not username or not password:
        print(f"ERROR: empty zoneedit credentials (current directory: {os.getcwd()}).")
        sys.exit(1)

    domain = match[1] if (match := re.search(r'^(?:.+\.)?(.+\..+)$', certbot_domain)) else None

    if not domain:
        print(f"ERROR: invalid domain (CERTBOT_DOMAIN={certbot_domain}).", file=sys.stderr)
        sys.exit(1)

    # print(certbot_domain)
    # print(domain)
    # print(match[1] if (match := re.search(f"^(?:(.*)\\.)?{re.escape(domain)}$", certbot_domain)) else None)
    # print(str.join('.', filter(None, ('_acme-challenge', re.search(f"^(?:(.*)\\.)?{re.escape(domain)}$", certbot_domain)[1]))))
    # sys.exit(0)

    sess = requests.Session()


    #
    # LOGIN
    #

    res = sess.get('https://cp.zoneedit.com/login.php')

    if res.status_code != requests.codes.ok:
        print(f"ERROR: unexpected return code: expected 200, received {res.status_code} (GET https://cp.zoneedit.com/login.php).", file=sys.stderr)
        with open(f"/tmp/zoneedit_{domain}_{datetime.now().strftime('%Y%m%d%H%M%S')}.html", 'wb') as err:
            err.write(res.content)
        sys.exit(1)

    tree = html.fromstring(res.content)
    challenge = input_value(tree, 'login_chal')  # ex: 59448b2d2a4f5c0f4128923919434697
    form = {
        "login_chal": challenge,
        "login_hash": login_hash(username, password, challenge),  # ex: d7cd0a698f6866e16ce716af4c08f55f
        "login_user": username,
        "login_pass": password,
        "csrf_token": input_value(tree, 'csrf_token'),  # ex: 3964616265393864336166363462396365356134626234616239663362326234
        "login": "",  # empty
    }
    # print(form)

    # post login credentials
    res = sess.post('https://cp.zoneedit.com/home/', data=form)
    # note: this returns 302, but requests library follows redirects automatically

    if res.status_code != requests.codes.ok:
        print(f"ERROR: unexpected return code: expected 200, received {res.status_code} (POST https://cp.zoneedit.com/home/).", file=sys.stderr)
        with open(f"/tmp/zoneedit_{domain}_{datetime.now().strftime('%Y%m%d%H%M%S')}.html", 'wb') as err:
            err.write(res.content)
        sys.exit(1)

    # change management to given domain
    sess.get(f"https://cp.zoneedit.com/manage/domains/?LOGIN={domain}")

    if res.status_code != requests.codes.ok:
        print(f"ERROR: unexpected return code: expected 200, received {res.status_code} (GET https://cp.zoneedit.com/manage/domains/?LOGIN={domain}).", file=sys.stderr)
        with open(f"/tmp/zoneedit_{domain}_{datetime.now().strftime('%Y%m%d%H%M%S')}.html", 'wb') as err:
            err.write(res.content)
        sys.exit(1)

    try:

        #
        # UPDATE TXT RECORD
        #

        # GET https://cp.zoneedit.com/manage/domains/txt/edit.php

        res = sess.get('https://cp.zoneedit.com/manage/domains/txt/edit.php')

        if res.status_code != requests.codes.ok:
            print(f"ERROR: unexpected return code: expected 200, received {res.status_code} (GET https://cp.zoneedit.com/manage/domains/txt/edit.php).", file=sys.stderr)
            with open(f"/tmp/zoneedit_{domain}_{datetime.now().strftime('%Y%m%d%H%M%S')}.html", 'wb') as err:
                err.write(res.content)
            sys.exit(1)

        # POST https://cp.zoneedit.com/manage/domains/txt/edit.php

        tree = html.fromstring(res.content)

        form = {
            "MODE": "edit",
            "csrf_token": input_value(tree, 'csrf_token'),
            "next": "",
            "multipleTabFix": input_value(tree, 'multipleTabFix')
        }
        idx = 0
        while True:
            key_host = f"TXT::{idx}::host"
            key_txt  = f"TXT::{idx}::txt"
            key_ttl  = f"TXT::{idx}::ttl"
            val_host = input_value(tree, key_host)
            val_txt  = input_value(tree, key_txt)
            val_ttl  = input_value(tree, key_ttl)
            if not val_host:
                break  # no more defined records
            if val_host.startswith('_acme-challenge'):
                # clear existing '_acme-challenge' records
                val_host = ''
                val_txt = ''
                val_ttl = ''
            form[key_host] = val_host
            form[key_txt]  = val_txt
            form[key_ttl]  = val_ttl
            idx += 1
        # add new record
        form[f"TXT::{idx}::host"] = str.join('.', filter(None, ('_acme-challenge', re.search(f"^(?:(.*)\\.)?{re.escape(domain)}$", certbot_domain)[1])))
        form[f"TXT::{idx}::txt"]  = certbot_value  # test: f"py-{datetime.now().strftime('%Y%m%d%H%M%S')}"
        form[f"TXT::{idx}::ttl"]  = ''
        # print(form)

        res = sess.post('https://cp.zoneedit.com/manage/domains/txt/edit.php', data=form)
        # print(res.text)

        if res.status_code != requests.codes.ok:
            print(f"ERROR: unexpected return code: expected 200, received {res.status_code} (POST https://cp.zoneedit.com/manage/domains/txt/edit.php).", file=sys.stderr)
            with open(f"/tmp/zoneedit_{domain}_{datetime.now().strftime('%Y%m%d%H%M%S')}.html", 'wb') as err:
                err.write(res.content)
            sys.exit(1)

        # POST https://cp.zoneedit.com/manage/domains/txt/confirm.php

        tree = html.fromstring(res.content)

        form = {
            "csrf_token": next(iter(tree.xpath('//input[@name="csrf_token"]/@value')), ""),  # ex: "3161623366623832366461613166656564343239646537303138653435623165"
            "confirm": "",
            "multipleTabFix": next(iter(tree.xpath('//input[@name="multipleTabFix"]/@value')), ""),  # ex: "eNpLtDKyqi62MrFSKi1OLVKyLrYyAzIzinITYZyU%2FNzEzDwQxxwio5ecoWRdCwABXxHq"
            "NEW_TXT": next(iter(tree.xpath('//input[@name="NEW_TXT"]/@value')), "")  # ex: "NEW_TXT":"a%3A1%3A%7Bs%3A15%3A%22_acme-challenge%22%3Ba%3A1%3A%7Bi%3A0%3Ba%3A3%3A%7Bs%3A5%3A%22rdata%22%3Bs%3A43%3A%22J50GNXkhGmKCfn-0LQJcknVGtPEAQ_U_WajcLXgqWqo%22%3Bs%3A10%3A%22geozone_id%22%3Bi%3A0%3Bs%3A3%3A%22ttl%22%3Bi%3A0%3B%7D%7D%7D"
        }
        # print(form)

        res = sess.post('https://cp.zoneedit.com/manage/domains/txt/confirm.php', data=form)
        # print(res.text)

        if res.status_code != requests.codes.ok:
            print(f"ERROR: unexpected return code: expected 200, received {res.status_code} (POST https://cp.zoneedit.com/manage/domains/txt/confirm.php).", file=sys.stderr)
            with open(f"/tmp/zoneedit_{domain}_{datetime.now().strftime('%Y%m%d%H%M%S')}.html", 'wb') as err:
                err.write(res.content)
            sys.exit(1)

        # tree = html.fromstring(res.content)

    finally:

        #
        # LOGOUT
        #

        res = sess.get('https://cp.zoneedit.com/logout.php')

        if res.status_code != requests.codes.ok:
            print(f"ERROR: unexpected return code: expected 200, received {res.status_code} (GET https://cp.zoneedit.com/logout.php).", file=sys.stderr)
            with open(f"/tmp/zoneedit_{domain}_{datetime.now().strftime('%Y%m%d%H%M%S')}.html", 'wb') as err:
                err.write(res.content)

    # wait for the change to propagate over to DNS
    time.sleep(25)